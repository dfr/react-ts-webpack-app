import React = require('react');
import update = require('react-addons-update');

interface ITodo {
    description: string;
    key: number;
}

export interface IMainState {
    newItem?: {
        description: string;
    };
    todoList?: ITodo[];
}

export interface IMainProps {}

export class App extends React.Component<IMainProps, IMainState> {

    state: IMainState = {newItem: {description: ''}, todoList: []}

    constructor () {
        super();
        this.changeName = this.changeName.bind(this);
    }

    changeName(evt: any) : boolean {
        var val = evt.target.value;
		var newState = update(this.state, { newItem : { description: { $set: val } } });
		this.setState(newState);
		return true;
    }
    addItem: () => void;

    render () {
        var todoItems = '';
        return (
            <div>
                <div>
                    <input type="text" placeholder="input new item" value={this.state.newItem.description} onChange={this.changeName} />
                    <button onClick={this.addItem} >add</button>
                </div>
                <ul>{todoItems}</ul>
            </div>
        );
    }
}
