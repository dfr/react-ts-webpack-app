import {App} from './app'
import React = require('react');
import ReactDOM = require('react-dom');

ReactDOM.render(
    React.createElement(App),
    document.getElementById('example')
);

