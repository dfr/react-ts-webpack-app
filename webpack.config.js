var webpack = require('webpack');
require('es6-promise').polyfill();
var ExtractTextPlugin = require("extract-text-webpack-plugin");
module.exports = {  
  entry: ['./src/boot.ts', './css/main.less'],
  output: {
    path: './public',
    filename: 'bundle.js'
  },
  devtool: 'source-map',
  resolve: {
    extensions: ['', '.webpack.js', '.web.js', '.ts', '.tsx', '.js']
  },
  plugins: [
    //new webpack.optimize.UglifyJsPlugin()
      new ExtractTextPlugin("bundle.css")
  ],
  module: {
    loaders: [
      { test: /\.tsx?$/, loader: 'ts-loader' },
      { test: /\.less$/,
        loader: ExtractTextPlugin.extract("style-loader", "css-loader!less-loader")
      }
    ]
  }
}
